import 'dart:convert';
import 'package:get/get.dart';
import 'package:project_kelas/config/api.dart';
import 'package:project_kelas/event/event_pref.dart';
import 'package:project_kelas/model/dosen.dart';
import 'package:project_kelas/model/user.dart';
import 'package:project_kelas/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:project_kelas/screen/login.dart';
import 'package:project_kelas/widget/info.dart';
import 'package:project_kelas/Loginpage/login_google_page.dart';

import '../model/arabica.dart';
import '../model/robusta.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 100), () {
            Get.off(
              LoginGooglePage(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Modul Robusta
  static Future<List<Robusta>> getRobusta() async {
    List<Robusta> listRobusta = [];

    try {
      var response = await http.get(Uri.parse(Api.getRobusta));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var robusta = responBody['robusta'];

          robusta.forEach((robusta) {
            listRobusta.add(Robusta.fromJson(robusta));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listRobusta;
  }

  static Future<String> AddRobusta(
      String rbstId, String rbstNama, String rbstAsal, String rbstChar) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addRobusta), body: {
        'rbstId': rbstId,
        'rbstNama': rbstNama,
        'rbstAsal': rbstAsal,
        'rbstChar': rbstChar,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Robusta Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateRobusta(
      String rbstId, String rbstNama, String rbstAsal, String rbstChar) async {
    try {
      var response = await http.post(Uri.parse(Api.updateRobusta), body: {
        'rbstId': rbstId,
        'rbstNama': rbstNama,
        'rbstAsal': rbstAsal,
        'rbstChar': rbstChar,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Robusta');
        } else {
          Info.snackbar('Gagal Update Robusta');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteRobusta(String rbstId) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteRobusta), body: {'rbstId': rbstId});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Robusta');
        } else {
          Info.snackbar('Gagal Delete Robusta');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Modul Arabica
  static Future<List<Arabica>> getArabica() async {
    List<Arabica> listArabica = [];

    try {
      var response = await http.get(Uri.parse(Api.getArabica));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var arabica = responBody['arabica'];

          arabica.forEach((arabica) {
            listArabica.add(Arabica.fromJson(arabica));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listArabica;
  }

  static Future<String> AddArabica(
      String rbcId, String rbcNama, String rbcAsal, String rbcChar) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addArabica), body: {
        'rbcId': rbcId,
        'rbcNama': rbcNama,
        'rbcAsal': rbcAsal,
        'rbcChar': rbcChar,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Arabica Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateArabica(
      String rbcId, String rbcNama, String rbcAsal, String rbcChar) async {
    try {
      var response = await http.post(Uri.parse(Api.updateArabica), body: {
        'rbcId': rbcId,
        'rbcNama': rbcNama,
        'rbcAsal': rbcAsal,
        'rbcChar': rbcChar,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Arabica');
        } else {
          Info.snackbar('Gagal Update Arabica');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteArabica(String rbcId) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteArabica), body: {'rbcId': rbcId});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Robusta');
        } else {
          Info.snackbar('Gagal Delete Robusta');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
