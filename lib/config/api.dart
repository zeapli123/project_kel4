class Api {
  static const _host = "http://192.168.240.53:8080/api_project_kel4";

  static String _user = "$_host/user";
  static String _robusta = "$_host/robusta";
  static String _arabica = "$_host/arabica";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // robusta
  static String addRobusta = "$_robusta/add_robusta.php";
  static String deleteRobusta = "$_robusta/delete_robusta.php";
  static String getRobusta = "$_robusta/get_robusta.php";
  static String updateRobusta = "$_robusta/update_robusta.php";

  // arabica
  static String addArabica = "$_arabica/add_arabica.php";
  static String deleteArabica = "$_arabica/delete_arabica.php";
  static String getArabica = "$_arabica/get_arabica.php";
  static String updateArabica = "$_arabica/update_arabica.php";
}
