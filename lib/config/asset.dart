import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Asset {
  static Color colorPrimary1 = Color.fromARGB(255, 56, 33, 30);
  static Color colorPrimary2 = Color.fromARGB(255, 74, 44, 41);
  static Color colorPrimary3 = Color.fromARGB(255, 91, 55, 57);
  static Color colorPrimary4 = Color.fromARGB(255, 112, 66, 65);
  static Color colorPrimary5 = Color.fromARGB(255, 254, 254, 210);
  static Color colorPrimary6 = Color.fromARGB(255, 250, 236, 193);
}
