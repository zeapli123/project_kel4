import 'package:get/get.dart';
import 'package:project_kelas/model/arabica.dart';

class CArabica extends GetxController {
  Rx<Arabica> _arabica = Arabica().obs;

  Arabica get arabica => _arabica.value;

  void setArabica(Arabica dataArabica) => _arabica.value = dataArabica;
}
