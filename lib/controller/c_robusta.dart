import 'package:get/get.dart';
import 'package:project_kelas/model/robusta.dart';

class CRobusta extends GetxController {
  Rx<Robusta> _robusta = Robusta().obs;

  Robusta get robusta => _robusta.value;

  void setRobusta(Robusta dataRobusta) => _robusta.value = dataRobusta;
}
