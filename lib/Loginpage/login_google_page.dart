import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
// import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:project_kelas/model/user.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_kelas/screen/admin/dashboard_admin.dart';
import 'package:project_kelas/ButtomBar.dart';
// import 'package:lottie/lottie.dart';

class LoginGooglePage extends StatefulWidget {
  const LoginGooglePage({Key? key}) : super(key: key);

  @override
  State<LoginGooglePage> createState() => _LoginGooglePageState();
}

class _LoginGooglePageState extends State<LoginGooglePage> {
  double getSmallDiameter(BuildContext context) =>
      MediaQuery.of(context).size.width * 2 / 3;
  double getBiglDiameter(BuildContext context) =>
      MediaQuery.of(context).size.width * 7 / 8;

  var _controllerUsername = TextEditingController();
  var _controllerPass = TextEditingController();
  var _fromKey = GlobalKey<FormState>();

  bool passwordobscured = true;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      child: Scaffold(
        body: Center(
          child: Positioned(
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.network(
                  "https://bptp-lampung-ppid.pertanian.go.id/assets/images/new_layout/logo.png",
                  // fit: BoxFit.cover,
                  height: 300,
                  width: 300,
                  alignment: Alignment.center,
                ),
                // Positioned(
                //   top: 0,
                //   width: MediaQuery.of(context).size.width,
                //   right: 0,
                //   child: Stack(children: [
                //     Image.asset(
                //       "assets/images/top1.png",
                //       alignment: Alignment.topRight,
                //       width: size.width,
                //     ),
                //   ]),
                // ),
                // Positioned(
                //   top: 0,
                //   right: 0,
                //   child: Stack(children: [
                //     Image.asset("assets/images/top2.png",
                //         width: size.width, alignment: Alignment.topRight),
                //   ]),
                // ),
                // Positioned(
                //   bottom: 0,
                //   left: 0,
                //   child: Stack(children: [
                //     Image.asset(
                //       "assets/images/bottom1.png",
                //       alignment: Alignment.bottomLeft,
                //       // width: double.infinity,
                //       width: size.width,
                //     ),
                //   ]),
                // ),
                // Positioned(
                //   bottom: 0,
                //   right: 0,
                //   child: Stack(children: [
                //     Image.asset(
                //       "assets/images/bottom2.png",
                //       width: size.width,
                //       alignment: Alignment.bottomRight,
                //     ),
                //   ]),
                // ),
                Stack(
                  children: <Widget>[
                    Positioned(
                      left: -getBiglDiameter(context) / 2,
                      top: -getBiglDiameter(context) / 2,
                      child: Container(
                        width: getBiglDiameter(context),
                        height: getBiglDiameter(context),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(100, 57, 7, 0.151),
                                Color.fromRGBO(82, 48, 3, 0.103)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                      ),
                    ),
                    Positioned(
                        top: 250,
                        left: 0,
                        child: Container(
                          margin: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                          alignment: Alignment.topCenter,
                          child: Text(
                            "LOGIN",
                            style: GoogleFonts.chakraPetch(
                              color: Colors.black,
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )),
                  ],
                ),

                // Stack(
                //   children: <Widget>[
                //     Positioned(
                //       left: -getBiglDiameter(context) / 7,
                //       top: -getBiglDiameter(context) / 7,
                //       child: Container(
                //         width: getBiglDiameter(context),
                //         height: getBiglDiameter(context),
                //         decoration: BoxDecoration(
                //           shape: BoxShape.circle,
                //           gradient: LinearGradient(
                //               colors: [
                //                 Color.fromRGBO(252, 8, 8, 0.329),
                //                 Color.fromRGBO(248, 112, 112, 0.219)
                //               ],
                //               begin: Alignment.topCenter,
                //               end: Alignment.bottomCenter),
                //         ),
                //       ),
                //     )
                //   ],
                // ),

                //
                //      Stack(
                //        children:     <Widget>[
                //        Positioned(
                //                 left: -getBiglDiameter(context) / 7,
                //             top: -getBiglDiameter(context) / 3,
                //            child: Container(
                //              child: Center(
                //                child: Text("UNIVERSITAS TEKNOKRAT INDONESIA",
                //                 style: TextStyle(
                //                 fontFamily: "Pacifico",
                //                 fontSize: 20,
                //                 color: Colors.white),
                //          ),
                //              ),
                //              width: getBiglDiameter(context),
                //     height: getBiglDiameter(context),
                //
                //                              decoration: const BoxDecoration(
                //                                  shape: BoxShape.circle,
                //                                  gradient: LinearGradient(
                //                                      colors: [Color(0xFFB226B2), Color(0xFFFF4891).withOpacity(opacity)],
                //                                      begin: Alignment.topCenter,
                //                                      end: Alignment.bottomCenter
                //                                  )),
                //            ),
                //        ),
                // ]
                //      ) ,
                //

                Stack(children: <Widget>[
                  Positioned(
                    right: -getBiglDiameter(context) / 2,
                    bottom: -getBiglDiameter(context) / 2,
                    child: Container(
                      width: getBiglDiameter(context),
                      height: getBiglDiameter(context),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(100, 57, 7, 0.151),
                                Color.fromRGBO(82, 48, 3, 0.103)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter)),
                    ),
                  ),
                ]),

                Stack(
                  children: <Widget>[
                    Positioned(
                      left: -getBiglDiameter(context) / 5,
                      bottom: -getBiglDiameter(context) / 7,
                      child: Container(
                        width: getBiglDiameter(context),
                        height: getBiglDiameter(context),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(238, 234, 234, 0.075),
                                Color.fromRGBO(250, 249, 249, 0.216)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                      ),
                    )
                  ],
                ),
                // Positioned(
                //     top: 20,
                //     child: Container(
                //       child: Text(
                //         " UNIVERSITAS "
                //             "TEKNOKRAT "
                //             "INDONESIA ",
                //         style: GoogleFonts.montserrat(
                //             color: Colors.red,
                //             fontSize: 20,
                //             fontWeight: FontWeight.bold),
                //       ),
                //     )),
                // Positioned(
                //   top: 0,
                //   right: 10,
                //   child: Column(
                //     children: [
                //       Container(
                //           margin: EdgeInsetsDirectional.fromSTEB(0, 80, 0, 0),
                //           alignment: Alignment.topCenter,
                //           height: 80,
                //           width: 80,
                //           decoration: BoxDecoration(
                //               image: DecorationImage(
                //                   image: AssetImage("assets/images/lg_1.jpg")),
                //               borderRadius: BorderRadius.circular(55),
                //               border: Border.all(
                //                   color: Colors.white,
                //                   style: BorderStyle.solid,
                //                   width: 2))),
                //     ],
                //   ),
                // ),

                Positioned(
                  top: 5,
                  left: 0,
                  right: 0,
                  child: Container(
                      // padding: EdgeInsets.fromLTRB(50, 10, 10, 50),
                      alignment: Alignment.topCenter,
                      height: 121,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "logo(1).png",
                            )),
                        //   borderRadius:
                        // BorderRadius.circular(10),
                      )),
                ),

                // Image.asset("assets/lg_1.jpg",),

                Form(
                  key: _fromKey,
                  child: Positioned(
                      bottom: 30,
                      left: 10,
                      right: 10,
                      top: 300,
                      child: SingleChildScrollView(
                        child: Card(
                          color: Colors.grey.withOpacity(0.20),
                          elevation: 3.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: TextFormField(
                                //bagian from login username
                                validator: (value) =>
                                    value == '' ? 'Jangan Kosong' : null,
                                controller: _controllerUsername,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                decoration: InputDecoration(
                                  hintText: 'username',
                                  hintStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  filled: true,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                      width: 1,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 2,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.green,
                                      width: 1,
                                    ),
                                  ),
                                  prefixIcon: Icon(
                                    Icons.people,
                                    color: Colors.yellow,
                                  ),
                                ),
                              ),
                              // Text(
                              //   "Cari tempat ngopi, sekarang !",
                              //   style: GoogleFonts.montserrat(
                              //       fontSize: 20,
                              //       color: Colors.white,
                              //       fontWeight: FontWeight.bold),
                              // ),
                            ),
                            // SizedBox(
                            //   height: 15,
                            // ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: TextFormField(
                                //bagian form login password
                                validator: (value) =>
                                    value == '' ? 'Jangan Kosong' : null,
                                controller: _controllerPass,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                obscureText: passwordobscured,
                                decoration: InputDecoration(
                                  hintText: 'Password',
                                  counterText: 'Forgot password?',
                                  counterStyle: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  suffixIcon: InkWell(
                                    onTap: _togglePasswordView,
                                    child: Icon(
                                      passwordobscured
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: Colors.yellowAccent,
                                    ),
                                  ),
                                  // Icon(Icons.visibility_off, color: Colors.white),

                                  hintStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  filled: true,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                      width: 1,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 2,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.green,
                                      width: 1,
                                    ),
                                  ),
                                  prefixIcon: Icon(
                                    Icons.vpn_key,
                                    color: Colors.yellow,
                                  ),
                                ),
                              ),
                            ),

                            //                           ElevatedButton(onPressed: ()
                            //                           {
                            // //                                   String _controllerUsername ='admin';
                            // //  String  _controllerPass = '12345';
                            //                             if (_fromKey.currentState!.validate()) {

                            //                               if(_controllerUsername.'admin' == _controllerUsername && _controllerPass.12345 == _controllerPass){

                            //                               }
                            //                                 // EventDb.login(_controllerUsername.text,
                            //                                 //     _controllerPass.text);
                            //                                 //   _controllerUsername.clear();
                            //                                 //     _controllerPass.clear();
                            //                                   }
                            //                           },

                            //                            child:Text('LOGIN')),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.blue),
                                width: double.infinity,
                                child: InkWell(
                                  onTap: () {
                                    String _controllerUsername = 'admin';
                                    String _controllerPass = '12345';
                                    if (_fromKey.currentState!.validate()) {
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                const ButtomBar(),
                                          ),
                                          (route) => false);
                                      EventDb.login(
                                          _controllerUsername = 'admin',
                                          _controllerPass = '12345');
                                    }
                                  },
                                  // onPressed:(){},
                                  borderRadius: BorderRadius.circular(10),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 30, vertical: 12),
                                    child: Text(
                                      'Sign in',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            // // SizedBox(
                            //   height: 3,
                            // ),

                            Row(children: [
                              //bagain pemberian text or continue with
                              Expanded(
                                child: Divider(
                                  color: Colors.white,
                                  height: 50,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Text(
                                  "Or continue with",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17),
                                ),
                              ),
                              Expanded(
                                child: Divider(
                                  color: Colors.white,
                                  height: 50,
                                ),
                              ),
                            ]),
                            SizedBox(height: 3),
                            Row(
                              //bagain pemberian logo login google,github,facebook
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    InkWell(
                                      onTap: () {},
                                      child: Container(
                                          // margin: EdgeInsetsDirectional.fromSTEB(0, 100, 0, 0),
                                          // alignment: Alignment.topCenter,
                                          height: 55,
                                          width: 55,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      "assets/images/logo_google.jpg")),
                                              borderRadius:
                                                  BorderRadius.circular(55),
                                              border: Border.all(
                                                color: Colors.white,
                                                style: BorderStyle.solid,
                                                // width: 2
                                              ))),
                                    ),
                                    Text(
                                      'google',
                                      style: TextStyle(
                                          color: Colors.white70,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                                // Column(
                                //   children: [
                                //     InkWell(
                                //       onTap: () {},
                                //       child: Container(
                                // margin: EdgeInsetsDirectional.fromSTEB(0, 100, 0, 0),
                                // alignment: Alignment.topCenter,
                                // height: 55,
                                // width: 55,
                                // decoration: BoxDecoration(
                                //     image: DecorationImage(
                                //         image: AssetImage(
                                //       "",
                                //     )),
                                //           borderRadius:
                                //               BorderRadius.circular(55),
                                //           border: Border.all(
                                //             color: Colors.white,
                                //             style: BorderStyle.solid,
                                //             // width: 2
                                //           ))),
                                // ),
                                // Text('',
                                //         style: TextStyle(
                                //             color: Colors.white70,
                                //             fontSize: 15,
                                //             fontWeight: FontWeight.bold)),
                                //   ],
                                // ),

                                Column(
                                  children: [
                                    InkWell(
                                      onTap: () {},
                                      child: Container(
                                          // margin: EdgeInsetsDirectional.fromSTEB(0, 100, 0, 0),
                                          // alignment: Alignment.topCenter,
                                          height: 55,
                                          width: 55,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      "assets/images/facebook1.jpg")),
                                              borderRadius:
                                                  BorderRadius.circular(55),
                                              border: Border.all(
                                                color: Colors.white,
                                                style: BorderStyle.solid,
                                                // width: 2
                                              ))),
                                    ),
                                    Text('Facebook',
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),

                                // _loginWithButton(images: 'images/google.png'),
                                // _loginWithButton(
                                //     images: 'images/github.png',
                                //     isActive: true),
                                // _loginWithButton(images: 'images/facebook.png'),
                              ],
                            ),
                            // Padding(
                            //   padding:
                            //       const EdgeInsets.only(left: 16, right: 16, bottom: 8),
                            //   child: Text(
                            //       "Untuk menikmati semua fitur kami, Anda perlu terhubung terlebih dahulu",
                            //       textAlign: TextAlign.center,
                            //       style: GoogleFonts.montserrat(
                            //           fontSize: 12, color: Colors.white)),
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 16, right: 16, bottom: 12),
                            //   child: Card(
                            //     child: Padding(
                            //       padding: const EdgeInsets.all(14.0),
                            //       child: Row(
                            //         mainAxisAlignment: MainAxisAlignment.center,
                            //         children: [
                            //           Image.asset(
                            //             "assets/logo_google.jpg",
                            //             width: 30,
                            //           ),
                            //           SizedBox(width: 30),
                            //           Text("Login Sekarang",
                            //               style: GoogleFonts.montserrat(fontSize: 20))
                            //
                            //         ],
                            //       ),
                            //     ),
                            //   ),
                            // )
                          ]),
                        ),
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _togglePasswordView() {
    // if (passwordobscured == true) {
    //   passwordobscured = false;
    // } else {
    //   passwordobscured = true;
    // }
    setState(() {
      passwordobscured = !passwordobscured;
    });
  }
}
