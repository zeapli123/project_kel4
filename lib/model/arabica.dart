class Arabica {
  String? rbcId;
  String? rbcNama;
  String? rbcAsal;
  String? rbcChar;

  Arabica({
    this.rbcId,
    this.rbcNama,
    this.rbcAsal,
    this.rbcChar,
  });

  factory Arabica.fromJson(Map<String, dynamic> json) => Arabica(
        rbcId: json['rbcId'],
        rbcNama: json['rbcNama'],
        rbcAsal: json['rbcAsal'],
        rbcChar: json['rbcChar'],
      );

  Map<String, dynamic> toJson() => {
        'rbcId': this.rbcId,
        'rbcNama': this.rbcNama,
        'rbcAsal': this.rbcAsal,
        'rbcChar': this.rbcChar,
      };
}
