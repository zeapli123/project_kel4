class Robusta {
  String? rbstId;
  String? rbstNama;
  String? rbstAsal;
  String? rbstChar;

  Robusta({
    this.rbstId,
    this.rbstNama,
    this.rbstAsal,
    this.rbstChar,
  });

  factory Robusta.fromJson(Map<String, dynamic> json) => Robusta(
        rbstId: json['rbstId'],
        rbstNama: json['rbstNama'],
        rbstAsal: json['rbstAsal'],
        rbstChar: json['rbstChar'],
      );

  Map<String, dynamic> toJson() => {
        'rbstId': this.rbstId,
        'rbstNama': this.rbstNama,
        'rbstAsal': this.rbstAsal,
        'rbstChar': this.rbstChar,
      };
}
