import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/model/user.dart';
import 'package:project_kelas/screen/admin/dashboard_admin.dart';
import 'package:project_kelas/Loginpage/login_google_page.dart';
import 'screen/login.dart';
import 'package:project_kelas/ButtomBar.dart';
import 'package:project_kelas/home_page.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: Asset.colorPrimary3,
        scaffoldBackgroundColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(builder: (context, AsyncSnapshot<User?> snapshot) {
        return LoginGooglePage();
      }),
    );
  }
}
