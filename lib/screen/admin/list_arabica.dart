import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/arabica.dart';
import 'package:project_kelas/screen/admin/add_update_arabica.dart';

import '../../model/arabica.dart';

class ListArabica extends StatefulWidget {
  @override
  State<ListArabica> createState() => _ListArabicaState();
}

class _ListArabicaState extends State<ListArabica> {
  List<Arabica> _listArabica = [];

  void getArabica() async {
    _listArabica = await EventDb.getArabica();

    setState(() {});
  }

  @override
  void initState() {
    getArabica();
    super.initState();
  }

  void showOption(Arabica? arabica) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateArabica(arabica: arabica))
            ?.then((value) => getArabica());
        break;
      case 'delete':
        EventDb.deleteArabica(arabica!.rbcId!).then((value) => getArabica());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Arabica'),
        backgroundColor: Asset.colorPrimary1,
      ),
      body: Stack(
        children: [
          _listArabica.length > 0
              ? ListView.builder(
                  itemCount: _listArabica.length,
                  itemBuilder: (context, index) {
                    Arabica arabica = _listArabica[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(arabica.rbcNama ?? ''),
                      subtitle: Text(arabica.rbcChar ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(arabica),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdateArabica())?.then((value) => getArabica()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorPrimary2,
            ),
          )
        ],
      ),
    );
  }
}
