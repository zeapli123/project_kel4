import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_pref.dart';
import 'package:project_kelas/screen/admin/detail_page.dart';
import 'package:project_kelas/screen/admin/list_arabica.dart';
import 'package:project_kelas/screen/admin/list_robusta.dart';
import 'package:project_kelas/screen/login.dart';
import 'package:project_kelas/Loginpage/login_google_page.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_kelas/event/event_db.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  get isClicked => false;

  bool favorite = false;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          // Row(children: [
          //   Text(
          //     'Hi!\nWhat would \nyou like to drink?',
          //     textAlign: TextAlign.left,
          //     style: TextStyle(
          //       fontSize: 30,
          //     ),
          //   ),
          // ]),
          SizedBox(
            height: 20,
          ),
          Container(
            child: Column(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.grey[200], // Warna latar belakang
                    borderRadius: BorderRadius.circular(
                        50.0), // Mengatur radius sudut kotak
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Search...',
                        border: InputBorder.none, // Menghilangkan border input
                        prefixIcon: Icon(
                          Icons.search,
                          color: isClicked ? Colors.brown : Colors.black,
                        ), // Ikona pencarian di sebelah kiri input
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Asset.colorPrimary3,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Image(
                image: NetworkImage(
                    'https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjt8mfAonOHn9vTYrkArsM1sobEpSjDKrv6PsHKNIQBZKrYJW733oWq7s0sRIwHpqQz1aBMO5XLBaex7k562Mk9SkClVIUsiEjfha-1knuuyL7bvIXyBYjnsqq3sUOI9NUlji2Vt23p31yOJF07ztNJOloFKi2UeQpHRI5qLpBviI-Yl6YHz_nTp9S4/s1488/WhatsApp%20Image%202023-02-01%20at%2013.50.52.jpeg')),
          ),
          SizedBox(height: 10),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(
                  5)), // Warna latar belakang/ Mengatur radius sudut kotak
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5), // Warna bayangan
                  spreadRadius: 2, // Jarak penyebaran bayangan
                  blurRadius: 5, // Tingkat kabur bayangan
                  offset: Offset(0, 0), // Posisi bayangan pada sumbu x dan y
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.discount, // Ganti dengan ikon yang sesuai
                  color: Asset.colorPrimary3, // Warna ikon
                  size: 20, // Ukuran ikon
                ),
                Text(
                  '1 Promo is available!',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            height: 40,
          ),
          Container(
            padding: EdgeInsets.only(top: 25, bottom: 10),
            child: Text(
              'Type of Coffee',
              style: TextStyle(
                  color: Asset.colorPrimary3,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            alignment: Alignment.topLeft,
          ),
          Container(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      'Data Pegawai',
                      Icons.coffee_maker,
                      0,
                    ),
                    IconButton('Profil', Icons.coffee_maker, 1),
                    IconButton('Logout', Icons.logout_outlined, 99),
                  ],
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              'Hot',
              style: TextStyle(
                  color: Asset.colorPrimary3,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            alignment: Alignment.topLeft,
          ),
          SizedBox(
            height: 140,
            child: ListView(
              children: [
                CarouselSlider(
                  items: [
                    SlideItem(
                        'https://down-ws-id.img.susercontent.com/0a6eeaf1c99d1871726b2983187c0de4'),
                    SlideItem(
                        'https://down-ws-id.img.susercontent.com/c8bbff8e90075c941747b7692eafcf3c'),
                    SlideItem(
                        'https://down-ws-id.img.susercontent.com/id-11134210-23020-n85r2u7sqsnv4a.webp'),
                    SlideItem(
                        'https://down-ws-id.img.susercontent.com/24f9b1811e90aed2549cec9b389b47bc'),
                  ],
                  options: CarouselOptions(
                    height: 140.0,
                    enlargeCenterPage: true,
                    autoPlay: true,
                    aspectRatio: 16 / 9,
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enableInfiniteScroll: true,
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    viewportFraction: 0.8,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 30),
          Row(
            children: [
              Container(
                // margin: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
                width: 165,
                height: 190,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 10,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/images/kopi2.png",
                          height: 80,
                        ),
                      ),
                      Text(
                        "Americano",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "arabika",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      RatingBar.builder(
                        initialRating: 4,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 16,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (index) {},
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "\$10",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                          Stack(
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    favorite = !favorite;
                                  });
                                },
                                child: Center(
                                  child: favorite == false
                                      ? Icon(
                                          Icons.favorite,
                                          color: Colors.grey,
                                          size: 28,
                                        )
                                      : Icon(Icons.favorite, color: Colors.red),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(width: 12),
              Container(
                // margin: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
                width: 160,
                height: 190,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 10,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/images/kopi1.png",
                          height: 80,
                        ),
                      ),
                      Text(
                        "Coffe Late",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "robusta",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      RatingBar.builder(
                        initialRating: 4,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 16,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (index) {},
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "\$10",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                          Stack(
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    favorite = !favorite;
                                  });
                                },
                                child: Center(
                                  child: favorite == false
                                      ? Icon(
                                          Icons.favorite,
                                          color: Colors.grey,
                                          size: 28,
                                        )
                                      : Icon(Icons.favorite, color: Colors.red),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DetailPage()));
                },
                child: Container(
                  margin: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
                  width: 160,
                  height: 190,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 3,
                        blurRadius: 10,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/images/kopi43.png",
                            height: 80,
                          ),
                        ),
                        Text(
                          "Ekspresso",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          "Gayo",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        RatingBar.builder(
                          initialRating: 4,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize: 16,
                          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (index) {},
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "\$10",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            Stack(
                              children: [
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      favorite = !favorite;
                                    });
                                  },
                                  child: Center(
                                    child: favorite == false
                                        ? Icon(
                                            Icons.favorite,
                                            color: Colors.grey,
                                            size: 28,
                                          )
                                        : Icon(Icons.favorite,
                                            color: Colors.red),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 8),
              Container(
                margin: EdgeInsetsDirectional.fromSTEB(12, 0, 0, 0),
                width: 160,
                height: 190,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 10,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/images/kopi4.png",
                          height: 80,
                        ),
                      ),
                      Text(
                        "White Copi",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Luwak",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      RatingBar.builder(
                        initialRating: 4,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 16,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (index) {},
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "\$10",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                          Stack(
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    favorite = !favorite;
                                  });
                                },
                                child: Center(
                                  child: favorite == false
                                      ? Icon(
                                          Icons.favorite,
                                          color: Colors.grey,
                                          size: 28,
                                        )
                                      : Icon(Icons.favorite, color: Colors.red),
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          Card(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            elevation: 10,
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const DetailPage()));
              },
              splashColor: Colors.blue,
              child: Column(
                children: [
                  Container(
                    child: SizedBox(
                      width: double.infinity,
                      height: 150,
                      child: Image.network(
                        "https://www.nescafe.com/id/sites/default/files/Memiliki-Tingkat-Keasaman-yang-Lebih-Tinggi%2C-Berikut-Cara-Menyeduh-Kopi-Hitam-Arabika.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                      top: 0,
                      left: 10,
                      child: SizedBox(
                        height: 70,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Toko Kopi",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Icon(Icons.star, color: Colors.amberAccent),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "5.0",
                                    style: GoogleFonts.montserrat(fontSize: 12),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Icon(Icons.access_time, color: Colors.grey),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "10.00-23.00",
                                    style: GoogleFonts.montserrat(fontSize: 12),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SlideItem extends StatelessWidget {
  final String img;

  SlideItem(this.img);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey.shade300)),
      child: Image(
          image: NetworkImage(
            img,
          ),
          fit: BoxFit.cover),
    );
  }
}

class IconButton extends StatelessWidget {
  final String nameLabel;
  final IconData iconLabel;
  final int index;

  IconButton(this.nameLabel, this.iconLabel, this.index);

  List<Map> _fragment = [
    {'title': 'Daftar Robusta', 'view': ListRobusta()},
    {'title': 'Daftar Arabica', 'view': ListArabica()},
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            // margin: EdgeInsets.only(bottom: 5),
            child: Material(
              borderRadius: BorderRadius.all(
                Radius.circular(40),
              ),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.all(
                  Radius.circular(40),
                ),
                onTap: () {
                  if (index == 99) {
                    EventPref.clear();
                    Get.off(LoginGooglePage());
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => _fragment[index]['view']),
                    );
                  }
                },
                child: Container(
                  // margin: EdgeInsets.all(5),
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    color: Asset.colorPrimary3,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  child: Center(
                    child: Stack(
                      children: [
                        Icon(
                          iconLabel,
                          color: Colors.white,
                          size: 40,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Text(
              nameLabel,
              style: TextStyle(fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}
