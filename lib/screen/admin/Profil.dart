import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/src/rendering/box.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:project_kelas/Loginpage/login_google_page.dart';

class Profil extends StatelessWidget {
  const Profil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const LoginGooglePage()));
            },
            icon: const Icon(Icons.chevron_left)),
        title: Text("Profile"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(children: [
            SizedBox(
              width: 120,
              height: 120,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.asset("assets/images/lg_1.jpg"),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text("REZ"),
            Text(
              "tugaskelompok.gmail.com",
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 30,
              child: ElevatedButton(
                  onPressed: () {
                    const LoginGooglePage();
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.brown,
                  ),
                  child: Text("Edit profile")),
            ),
            SizedBox(
              height: 20,
            ),
            Divider(),
            SizedBox(
              height: 10,
            ),
            ListTile(
              onTap: () {},
              leading: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: Icon(
                  Icons.settings,
                  color: Colors.blue,
                ),
              ),
              title: Text("Settings"),
              trailing: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.black.withOpacity(0.1),
                ),
                child: Icon(Icons.navigate_next, color: Colors.grey),
              ),
            ),
            ListTile(
              onTap: () {},
              leading: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: Icon(
                  Icons.wallet,
                  color: Colors.blue,
                ),
              ),
              title: Text("filling Detail"),
              trailing: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.black.withOpacity(0.1),
                ),
                child: Icon(Icons.navigate_next, color: Colors.grey),
              ),
            ),
            ListTile(
              onTap: () {},
              leading: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: Icon(
                  Icons.supervised_user_circle,
                  color: Colors.blue,
                ),
              ),
              title: Text("User management"),
              trailing: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.black.withOpacity(0.1),
                ),
                child: Icon(Icons.navigate_next, color: Colors.grey),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            ListTile(
              onTap: () {},
              leading: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: Icon(
                  Icons.info,
                  color: Colors.blue,
                ),
              ),
              title: Text("Information"),
              trailing: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.black.withOpacity(0.1),
                ),
                child: Icon(Icons.navigate_next, color: Colors.grey),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LoginGooglePage()));
              },
              leading: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.blue.withOpacity(0.1),
                ),
                child: Icon(
                  Icons.close,
                  color: Colors.blue,
                ),
              ),
              title: Text(
                "Logout",
                style: TextStyle(color: Colors.red),
              ),
              trailing: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.black.withOpacity(0.1),
                ),
                child: Icon(Icons.navigate_next, color: Colors.grey),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
