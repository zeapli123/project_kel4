import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/screen/admin/list_robusta.dart';
import 'package:project_kelas/widget/info.dart';

import '../../model/robusta.dart';

class AddUpdateRobusta extends StatefulWidget {
  final Robusta? robusta;
  AddUpdateRobusta({this.robusta});

  @override
  State<AddUpdateRobusta> createState() => _AddUpdateRobustaState();
}

class _AddUpdateRobustaState extends State<AddUpdateRobusta> {
  var _formKey = GlobalKey<FormState>();
  var _controllerId = TextEditingController();
  var _controllerNama = TextEditingController();
  var _controllerAsal = TextEditingController();
  var _controllerChar = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.robusta != null) {
      _controllerId.text = widget.robusta!.rbstId!;
      _controllerNama.text = widget.robusta!.rbstNama!;
      _controllerAsal.text = widget.robusta!.rbstAsal!;
      _controllerChar.text = widget.robusta!.rbstChar!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.robusta != null
            ? Text('Update Robusta')
            : Text('Tambah Robusta'),
        backgroundColor: Asset.colorPrimary1,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.robusta == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerId,
                  decoration: InputDecoration(
                      labelText: "ID",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNama,
                  decoration: InputDecoration(
                      labelText: "Nama Kopi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerAsal,
                  decoration: InputDecoration(
                      labelText: "Asal Kopi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerChar,
                  decoration: InputDecoration(
                      labelText: "Karakter Kopi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.robusta == null) {
                        String message = await EventDb.AddRobusta(
                          _controllerId.text,
                          _controllerNama.text,
                          _controllerAsal.text,
                          _controllerChar.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerId.clear();
                          _controllerNama.clear();
                          _controllerAsal.clear();
                          _controllerChar.clear();
                          Get.off(
                            ListRobusta(),
                          );
                        }
                      } else {
                        EventDb.UpdateRobusta(
                          _controllerId.text,
                          _controllerNama.text,
                          _controllerAsal.text,
                          _controllerChar.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.robusta == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorPrimary2,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
