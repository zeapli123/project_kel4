import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/robusta.dart';
import 'package:project_kelas/screen/admin/add_update_robusta.dart';

import '../../model/robusta.dart';
import 'add_update_robusta.dart';

class ListRobusta extends StatefulWidget {
  @override
  State<ListRobusta> createState() => _ListRobustaState();
}

class _ListRobustaState extends State<ListRobusta> {
  List<Robusta> _listRobusta = [];

  void getRobusta() async {
    _listRobusta = await EventDb.getRobusta();

    setState(() {});
  }

  @override
  void initState() {
    getRobusta();
    super.initState();
  }

  void showOption(Robusta? robusta) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateRobusta(robusta: robusta))
            ?.then((value) => getRobusta());
        break;
      case 'delete':
        EventDb.deleteRobusta(robusta!.rbstId!).then((value) => getRobusta());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Robusta'),
        backgroundColor: Asset.colorPrimary1,
      ),
      body: Stack(
        children: [
          _listRobusta.length > 0
              ? ListView.builder(
                  itemCount: _listRobusta.length,
                  itemBuilder: (context, index) {
                    Robusta robusta = _listRobusta[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(robusta.rbstNama ?? ''),
                      subtitle: Text(robusta.rbstChar ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(robusta),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdateRobusta())?.then((value) => getRobusta()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorPrimary2,
            ),
          )
        ],
      ),
    );
  }
}
