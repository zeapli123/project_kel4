import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/controller/c_user.dart';
import 'package:project_kelas/event/event_pref.dart';
import 'package:project_kelas/screen/admin/Profil.dart';
import 'package:project_kelas/screen/admin/home_screen.dart';

import '../../model/user.dart';

class DashboardAdmin extends StatefulWidget {
  const DashboardAdmin({Key? key}) : super(key: key);

  @override
  State<DashboardAdmin> createState() => _DashboardAdminState();
}

class _DashboardAdminState extends State<DashboardAdmin> {
  int _selectedIndex = 0;
  String _title = '';
  List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
  ];

  CUser _cUser = Get.put(CUser());
  int _index = 0;

  void getUser() async {
    User? user = await EventPref.getUser();

    if (user != null) {
      _cUser.setUser(user);
    }
  }

  @override
  void initState() {
    getUser();
    super.initState();
    _title = 'default';
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        child: AppBar(
          elevation: 1,
          toolbarHeight: 60,
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          centerTitle: true,
          title: _title == 'default'
              ? Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            // Container(
                            //   child: Text(
                            //     '',
                            //     overflow: TextOverflow.ellipsis,
                            //     maxLines: 1,
                            //     style: TextStyle(
                            //       fontSize: 12,
                            //       fontWeight: FontWeight.bold,
                            //       color: Colors.black,
                            //     ),
                            // ),
                            // ),
                            Text(
                              'BSIP Lampung',
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                      CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.black,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Profil()));
                          },
                          child: CircleAvatar(
                            radius: 27,
                            backgroundImage: NetworkImage(
                                'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQUExYUFBQXFhYXGR8cGRkZGSAhIRojIR4eHyAbHBwhHykkIR4oHyAhIjIiJiosLy8vICE1OjUuOSkuLy8BCgoKDg0OHBAQGzcnISYxLiwsNzQsLjQwLi43MC4sLi43LC4wLjAwLjAuLjEwLi4uNy4uLi4uLi4uLi4uLi4uLv/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAwQFBgcCAQj/xABDEAACAQIEBAQEAwUFBwQDAAABAhEDIQAEEjEFBkFREyJhcQcygZFCobEUI1LB0RVicpLwFiQzgqLh8UNTssIXY9P/xAAaAQADAQEBAQAAAAAAAAAAAAAAAgMBBAUG/8QAMBEAAgIBAwMCBQMEAwEAAAAAAAECEQMEEiExQVETIgUUYXGRgaHBYtHh8DKx8UL/2gAMAwEAAhEDEQA/ANxwYMGAAwYMGAAwYMGAAx5gw3qZkDa+NSbMboc4SaqBho1Unc44Jw6x+RHPwOHzJ6DCD5hj1+2OWOE5xRQQjkweoT1P3wgxwsRjkpiqpCMbnHqu3Qn74UNLAKZw1oXk5XOVB+I/W+FqfFWG6g+1sJFDjg0jhXCD7GqUl3JSjxJG3Ok+v9cPAZxXDTwpSqMvykjE5YF/8seOZ9yw4MRlDiQ2YR6jEgjgiQZGISg49S0ZKXQ7wYMGFGDBgwYADBgwYADBgwYADBgwYADBgwYAPMJ1aoUXxW+Z+d8rk2FOpUHikTpALaQdi2kGJ6DriJ4XzZTrVFRa9N2c2GzHc2Bg7emGirfIsn4LVWzBb0HbCReLmwxQ/iRzJXy5pUaB0NUBJe0xIAUTYXmT7YgDyXm65nM5n6FmqfkYA+mOfVfEMOl4m6EhjnkftVmk1eZcop0tmqAPY1Ut73w+Gdp+GaodTTALFwQRAEkyPTGb0/hzQAg1KpPcaQPtpP64r/FshW4azBKhajXVlPQNIgh121AGQw/qMcun+NYc09kev6j5NPkhHdJcFjzfxUYsRQy2pfwlmMn1KqLe04bHnriT/wDDyoHtRqt/PHfwtqfuqy9nB+6x/wDXHfF+fRRq1KXgFijFZLxMdY0nHBl+K6p5pYsULa+qRSOnh6anOVX9Dih8SM3RYDM5YaT2V6Z+mqQcWfiPxByyZZa9M+IzkqlLZgw3D76QJEm8yImcR3A+N0c/TdGpxEB6bXEHYg/T0IxQafBVXiIy7XQVQL9VjUAfdYB+uLab4tklvhkjUoq3348i5NPtUZQdp8E+nGOMZr95TJp0ztpCov0LSx97jHf9o8by/mJaqo6EJU/JfPiY5243VytOmaSr52I1ESFgWAHc/wAjis5H4h1l/wCLTRx/dlT/ADH5Y5MOu12ZerBLb4vn+xSeHBje2Td/sT+W+KS+C5q0CK6wFVSdL9ySRKx1F+kdYbUPiq4I8TKiP7tQgx6Arf74z6vTqMQ7gzVYkE/iJNyPSTjTviCwTJFe7Io+hB/Rcd+f4lkwzhj6uTr7fUhjw74ylfQk8z8QskgQ6ncuoMIslJ6PcAH0BJ/LDvJc5ZKr8uYRSelSU/8AkAPscZt8P+EU61WoaqB0RNjtLGx+wbFmzXI2UqEhC1NhuFeY9w0/yxuX47DDleOa6V2GhpMk4b40XtYYSpBB2IMj74Vy9Zk2P0xkmY5czeRmtlaxZRdgtjH95Lqw/P0xeuSuZRnKRJAWtTgOo2vs6+hg26EH0x6ml1+HVR9rs58mKeOVSVMu+VzQf0PbDjEBEGRviRyec1eVt/1/74rkxVyikMl8Mf4MGDEioYMGDAAYMGDAAYMGDAAYr3NHMKZddC1KYrtGhXdRAJgvBImLmOsYk+K58UaZY3PQdz2xGZjhlKt5qiSzASZIm3ocZuV0btdWZLxbkZ67vUFVS7HUzMdWsmZJZScKcjcoVstnErVimhFeCrE+YjSLFQdiTt0xNc38pt4qfsjPShSW8MkT2Jjf64rtWjxSjtWZo6OoM/dZw6YjRN/Fqkr0qNVSCUcoY7MJn7r+eJPM5l63DzUpsVc0dQKmCCBJAPuCMULNcZzeYVsvVSk1mYwpDL4YLnrHSNuuLj8Oc1rymg38N2W/Y+YfTzEfTHzvx+NKOWv+LT/39jo0iqbh5RnWTfMVKimkaj1Z8pBJM+p7e9sXv4m11GXpIY1tU1AegUgn2kgfXHnEueqdBnpU8uZRipkhBYxYKDb7YfcOq0OJ0GNSkFZTpPVltIKvAMX/AFxwZss98M+SG2K8NN89P0HhCO2WOMrk/wCCE+FlTzZhfRD9iw/nhLiuTzdLiFWvlqLkyNLaJU6kWd7G8/XHvw/pGlna9Ft1RlPrpdROJnmjm98rV8FaIeUDAlj1JEQB6d8bPJljrpPCk3KKfWvHIRjGWnXqOqbDkXgFWialSsAr1IAWQSLkkmLXPQYpnM/ENWeqVqR+R10sO6BRPtI+ow64nzjmq6lBppqbHQCCfTUTP2jEFlsu7OtOmNTtYAdfQTjt0ulyLJPPnq2q46JEMuWLjGGO6X/Zp3B+YMtnafhVQocjzUm2Pqh6/qPzxX+YuRWpg1csSyi5pm7D/Cfxexv74p2ay70201EZGHRgR+v8saT8POK1q1J1qksEICOdzMypPWIF974482KWiTzYJe3un0/Qtjms72ZFz2ZReDE1s1l1P/uIPoGBMDpbpi5fFGtFGin8VQn/ACqR/wDbEbkcmv8AbLBR5Vdnt605P/U2OvinW/eUE7Izfcgf/XFMuT1tbif9O78oSK2YJ35olvhlldOWep/7lQ/ZRH66sUTO5kVcxWreJ4ZLMyte9/KJW4t1xqPBUTLZKkKhCKtMFyTABa5k+7RiK/2OyFbzUmMf/rqgj89UY5tPrIQz5MmROm6TrikVyYJSxxhFrhX1Ffh7xOrXoP4rFij6Qx3IgGCepHf1xBcqVjQ4lmFpfL+8WOkBxFvTbFlzuey/D6GhIkA6EmWdj1PWO5/7DGVU+I6GLs5DMbkEgyTJ2vvjv+Excs880I1FtV/clqeIxg3bXU3ROLt+JAfYx/XCq8TQ7hh/r0xjOV5uqj5a5Po0N/8AIE4lstzvVHzLTf2lT95I/LH1KytHI8SZtfC+KpU8uoFunr9O+JXGI5PnWmCC1N1PdCGj1vpxpnKXM9HOIdDedIDqQQb7NB6H06ziTabtFUmlyWLBgwYw0MGDBgA8xyzQJOOsQXM2c0r4Y3dSSewBAv7zH0OFk6TY0Y7pJEBxfibVHJU+SIW3Tv8AX+mLR46AfOto6jFMyywR7joY37HEIHIUMCR5HNj3fTiOnTnbZ1a1xgoxS8mg0oauxBBGgXBnCnEFApv/AITipV6/hhD4miwBJRmmw7MCPe+GfMPGqlOhUjMUrECS7AzO2l00zY/ix0OJw2MOV+DuuazNWuoVWUpTuPMHbzH6BQP+bDD4b1PCr5jLk9PzRip++r8sMl5xzCwCEaRIlRcHYjSRb1xE0+OVKeZOZRVDGSVIOm4g9Z3vvjh+JYHnwPGuv+tfuNjl6eRTLbxzkh6+ZqVVqIiPBuCWmADaw3vviY4dk6HDqDFqliZZjuxiwVf0F8ULMc5ZurtWCD+4APzufzx5kMjSrMHzOaM9RDEn01sIH548ePw3VZoRx5Ze1Vwl4+pf1scZOUI8+WLcu8cRc81eqdC1PEJsTGq4FgT2GPeduK0q9enUoOTpQKTBWCGYiJAPXfEnxPhuSFMOsFU6q35GLkknr3xT2i5C6ZNhMx6SfTrj018Pisqy90q+lEN0tuy+LsWdwo7AY64Jxf8AZq4rKquQDZuxtII2PSb9cMMzlnqU3IViqQWIBIXtJ6YunLfw8LLl8wavzNqdGGyTbQR1Kgb9/S99VLD6dZHw+P2NjCUn7eq5JSh8QMs4irSqKe0Kw+8g/lhHiPxBpKunL0mLdC4CqPWAST7WxIUOQ6S1qzkI1BlXwqcElSB5hPbtfr6Xf8ucMyjuUGV+YMFrKoAA0aWGqZm56dZx5OD4Zo8r3R5XHFstPLnjw/zRQOT+LU6WZermHILKwnST5mZSSQB79Mdc156lmc5TK1AaUU0LmQANRLG4ERP5Y0rMfDfJvMioCYuHuPa36zhjnPhZljSCUnZHDSajSxIv5Y1BR0uB0x3v4fH1fVT5qjn3T27e12RPxE4in7KqoysKjgeUgiF83T1C4ofBuHCu5RHKuoBaVtGpUMENJuwtAxcsx8K66sND06q2kfKT3HaPrhPI8t1qOYNd6TorxqJEi7o1mUlYLAdRY4bRaCOnhsbvubkm8kraKrnOAVVrigj03Yx3DGRIgEaf+rEZwXhFZ6iCpBBdQDrVhcN1Unt1xfOKZKM9RrD+KnJ1AbNBsTe3bDbglA03qJpIGum20DyOV/R8ejGKj0JpIqvHuXKtOsyqoI8LUIg3Bvv6A4Z5PgdV2ZdDLCI1geoWRvG5xpfHABXBKgk0WuZ6B+xjpg4IV8WwImiLz2KjaPTvgyOotlMSuaTM2fhlddmb64ccq801spXp10OoAS6XGtJhl6ibWvuBjXjRQ7we8/8AcYoFTl+gR8q7VU8pIsBqgAx1JxHFl3Jt9jp1WFY6o3/h+dStSSrTOpKihlPcESMOsZp8LeOAf7m3lhdVITO3zLPrZo3u2NKxSMlJWjmPcGDBhgPDjJON8fFWu7q4VSIGx8ouIvaYnF0+InEzQyVQr89SKa/83zf9OrGWcOUXFUKzFSCBJhT/AHSIG8bxjk1MuKBcckzwrP5h6yys0yJUgWges77na/1x29Vwhm/7r8QB3qjuMRp4lRpKBTJ1AW9zIvsIgx9DhBOZCBBctYKdJiNjMdp364XT5447TRmeadK7LRx+o6oNEatYtpU2huhB9MRHG1Lpmop0XiqABUSVF6lyFg7Df1w5y3OpVWbQNTkW9JNr9fX1G2GPEOd69RQBSowPMf3eraQfmm0nptHaZt85CibrqHMXLlHx6SGihH7LJYEhlIR7L002gdd8V+ry/l1qZdFFRfEJjzkizwQwO++JhOcnYRUp06jRpDEMDEFYIVtP3B/O0aOMkstUov7uSqgWUXIAY3ufrYdsHzcK6MzcrGHMXCUy9bwxWI8oMeGCFkdbzOx2/XBwvgNSqyhK1BwUJMSGB80SsC21u2Fslw85pczmqzlQg1SADLdFjsAAI9VxAI5BkEgjqDBH1xOOqUpOK7f+mtyVPyWihkTmay5SkwVUDEsfxMoJP529yewwz5e4RVq5ynSeg+hGDVSynSANwxNrxEdZw35c4mctmKVZp0g39VMq0d4v9Rja6efpMocVEKkSDqER98cWs1uSDqK4a4+jOrBGE116EdkOG5fKq7UUhazAkTI2NgDsLm3rjivnWaPwxO3+u2GGb4jl6ZIOZo/55P2WcR9TmrJj/wBZ2/w0m/Vox8/PHqczumz0Fkww7onqGbZdr2gT0xLcPzoMlRp0+31xS15qyR/9Z1/xUm/lOHuW4plnPlzNG/8AE2k/ZgMEcWpwu9rGeXDPjci01ONVEiFDqRY3k/Wf5Yh+cObqlFUpquhqg3Uy3bSojf1/TErSzVIKIqJpA31CLes4yTnXj618wWphdNMaUqS0mOouABJMW2jHvaLWZJuMJLhLl+fB5+eMIJu+vQv3KdbwcvW8UFKtUk2A8oi2ohj5pJJOILN82U0pVaLQDqpkGm+stoZbERCyAZlp27DFGovmMwy0g1SoWMKpYn9THrJxOnk1V8tTOUadWJ0E7e5kH8sdubWwg0r5/L/COaO+S4RaafPhWiqUF06jJZgCb/hVSY36k+2LZzDlh+xK7KnijwyzBVBnUs7evScY7yoT45VDl9UHS1YkA3AhGkAEzNxJ/LFrz/EMxTTVUp5coTp1U8yp3tIWJOOuE1KNixfB7zEy+IpDDyoVbaBMjckCfNsfvhLgoC1qcMD+7cdj8zn2+xO2K5nqzI+t3BQMx0g6Rptpa0k7mZgW2x1wfjqh6TspAAcehEGDPeW2xk5qUWkPiklNP6oveZzegatDPOwX+eMvzmaLuztHhmq0kdDvAsJ3H3OLNns/VrxT0OmofKpjuLnc/pt64gm4LTpsRUqFipBKrcjr5m6Y48MVBe5nZrsqyNV0R7wjNvQzFPMKSdB1IqqSzC/k73H5H2GPo7JZtatNKiGUdQynuCJGPnVaVNNIBdSDq1swIMTGoFRYH16DpvqvwhzrHKvQcyaFQhT3RvMI9jqEdAAMdWOSfCOKLRfce4MGKjGX/FXiCNVo5VnZQUZjp6FrKdwejW9fTFBy+WdI8RLKLJrM9VkgE+hj1xK88cYpf2lXaoCCrLTRhFgqCTeb6ye23XEKOI5YhA2YAZREnUQ203PT7fpjiyN73w6EaZzxbPhqhJDNEgQR3suwmJiR7YY/tB3hhI1TA8vmHl1HuO3ce+PK1ZSWIaVIHzG1jYiDe1ze4HtjmighQXEhoMj1nzObiBfaffE9kUqolJExk85oEaabi0h7EbwJ+sdrHDPi2YLFYAU3haYOkttNwfS/W31QYgmFeEECYKg2mBtJvab4kaIoFXBOlTpXQz1N+mmZIJIn8Q3Md40ovdRliC5RA58Wp4RC2ZhBPuDMTft6G2GudfRqpqzFZ3MXAJv+np98S1TgbENUR2qKJ0rUmVi0GWEAAfNp2X3mv5imVYg/ht0tHtYX+nbGwlF9HYMtPHP92yFDL7PWPi1B1ixAP10j/lOIjlzgpzNSCdNJBqqv/CP6n+p6Ydjm+s1M069OlXEEAuokHoZAi3tPrh5Q5ooUaKUKWX8SmQfG8UwXJi8rPb7QOmOVLPDG4pe5tu0/Pft26HU3jlNNvhLp/B7zsKT0MrVoCKUVKaj0VgB+jHDavy4Dw+lmVXzgsanqpcgH6AD6E9sPuL1aFbhoaghprSrfIW1FdUzcmdJL4k6vFRlRkaLx4T0dNYHbzBBJ9jP0JxzrJkjjjGF2pPh9Wlbodwg5OT6NL8ukUzl7gdTM1NCWUfO8WUfzJ6DEtkOWqD061apmGpU6VZqYOmZAICkx1MjYYs/LvFKK1KlDLKBRoU2dn3LtIvPURN+vSABiG4DTpPwyqK9Q00av5nAkz5CLQdzjZ6rLJvrFXFLi3T/kI4caS7un9uCM4hyyq01r0cwtaiXCMwWCkkCSJ9fTcYj+aOEjLVzSDFwFBBIjf09xiV4pxLLUso+WyzvUNRtTuwiIg2sP4QPvfCnxKpzXpVY8tSkIPQmWMfYjF8WXJvipt07q1TdVX8k5whtbj1VC/wDstlhmaNE6ylegWViwkOLyIAHyg2IIwzzPAKuSzFKqw10VqKfEAtGoSGH4TH09cSvG65pNwuo3lZVUMDuBFMNP0Jxxxbj1XJ5ytTI8Sg5DGm3Zh5tJ6Xm2x/PHNCedtJO007Xmn2ZZxxrqqprnxasksnlRT4lmtMeI9HXS9zE/9QxQ+F5alUqMMzWNIAEsxUlmMiV/xbm87HFg5q42jVMtmstUAcKVI6rB2ZexDEdj0wlU5soMfEfI0mrdWmxPcjTv7k++KYI5VDdtdtJPpaa47+SeR43Kr6NvvTv7DbmXhlPKvQq0dRp1FDjX8xIIJkQIkEWjvj2vkaWvxabhho2AZdILBpMiDcAR+mGGczWYztadLVHNlSmpOkdgBMD1P3xPcC4JmaLnxqFRF8GqJZbCwYX23GPW0scix7JPnyRk1Kb29CP4nlKdUpSSoofUupRGpRvHv1xGZTK1FqKlO+l7a7ibXPbePviSznDKmXqiqyBlqxBiymJgkQZPf+6fYM8nK5lASQCzDrfzD3jbHRt9ONBFc0XOhliaIIAWq03LfL6kiDtAxQ85XqU2JdlIUwyhY07lSbkEECQZ9wDbGj1ckYAE/KSY6wRY+04p/N/DQKpIBCtlxqH/ADGD6GR+vfHLi5bs79Tjain44IGpm0qBbwDJJZu1wbkjuIAHTF0+E3MOniIoT+7rU3VJ7r5xJ6mA3Tr0xSczw1dJJpMERFBYnYnqe42j9cTHJSUaWYy1ZTqIqqZJJKgmGG0bSbDrvjptQ5ZwLyfSeDHk4MW3FD5k5t4Z4+ar1KL69VWo0EQJ1eZde1p6+uIZMv4TqdJlXIYmwA/jFxA9wREn0xYm5rBZwiJTuSZOrUZmT0M/c+skGo8R4lUNXWWMk6o6A32HTHNjc3xJC9SRp1z4hA1OYAUpJ99UDzbQBGFqbDQuhHVdwugSSsLLAmDtE9z0wvkcoiUkqCoP3izEzYEQXXobmCTO/Q47rZhWJ8JhEAyII8wgWgEEX6x+uFnLnhE5HiF0AVJIgk6mnSdpgGD1tfbHuYWoQhgrp+Riw17jzG8QIsABGEwUTzH5rBb6RI30tHlm9owZioWIV1UR+G5gRI9QpntY4mr7ExxleOV6WY8V2LgjQWgHaASImCAJjc/nh/xRKZZXoVJ1ixcMCYG5Jkfrt0nDHL1aQ+ZQQoGwKgHrIvIIsbdJ7YdZWqQFNUM1GJZZ1EAHyCZsoaAYjY9jiU4pPclT6fc0ZHJg6ZJUtsWB9oAkBjMG09O8Y5zeQVAPOSSNtP3+xt74sXHOOUfA0iqr12iFUSsarhhETveAdsRVesaqgrUhVFxoAKtZdKxbTCq47TeZxmOU3zJUDVET+ytq0wJJgCRf88d5o1T/AMQu2nyjUSYjoCdgO2FcxTLWXU2r5m+ZgbAFidxaN5sRtiTy/FtR1UAXIA8Wq8KzEj5ZAHltERuPvSV9UC6EXw7i1SiKi0yAKq6HkTa+3bc47Ti9QZZssNPhs+s28026zEWHTDn9qzi1G1EsiAKIUEsIm4aQWHy23jDilmy8kCiwNxqy9HUO4MpfDvBFrc67P9ew/uSpMr+J3h3OGZoUxTV1ZV+XWs6R2BkW95wouddZHgZWoRvOVpAD/px5ludPBqKr5LLnWwXVSGiBIk+WxN/Tb1w8tNHKqdNGQc0/a6IvO56tmampy1VzYACYHYKBYYkU5ez+ZOs0K7k21VJH51CLYlsh8XaMGcnGldXzg9QIEr64kafxXy5ptVXKEBGAcFgD5tiI3/LFIaaMaXj6GOM31I3/APG+aWmalZqdMCLSWa5A2Ajr3w5yXKdBfn1VD6kge4CkH7k4ks38VMpmKRpgVkaoYU6AxBUq2w3EGN++Gf8AaQNhmVns+XP6irGKbcceo8YV1JvhlQUARRimCIOi31t19cTB5iNSg1MjVqQqX1dSImI73jGZ5Pi7GoxLUqVMjdmPmNrBVJYHcgx0w4z/ABR6NPTKGAZI1Xk7QUF4v9PXD5JRihrJXmfJVK2WFOkwDqQ0kbwrAj3viocTqeZS7Cmf2gwwnZ53t81vvfD/ACHHWcG51EWj+mK1zVmYVqY1B0KsSDaDa3+bE4zWVU+GYnZr1NriWlgCNW03W8bSYxUOcXFWroZ0VCpQw41MQCQpHSSSB7/TEfnc8/gkAE+cXLHZgx9e2Iqoml6bFpu0JEDYCfU3xLDip2z0dTluG1IfcRpVhUBRUqIRTR1Y76UA0qy3U73B6SbDEXml0lwg0KpOkRAv1vH0PtjyvmtRBJgaRMHbr997dsOskqVidLEmPxGTuADaJ97W62xs5N8s83sbp/taPT74MZV/aDfwt/l/7YMb6hu5FQy+SqrUcOraxuSO53O8zEfriC4zTYVLhlsPmBGLvzJxuumbqogUBK1RBCgk6XKnVINpFp3+mKxnkq1iNTElbCWG3XSCRePvtjcblfuQ+7sMx4vg011/u6jEKAeoKhg33BjC2TdkR21dVAPlNgdPX0O3tODh9G6rpfUh1EEEiPKLeYReTMdu0YKtEjTKGY9e3t2xbgGOuGZtIIIAETDS2sxGgRZdz06dMSAqOpDmmG0yp0MwiZM6bQotNx+oxX6VFoHlYGJtPr6YXTOVS5Z2ctpHmJMwPKBttpMeuJyx2+BHAeoDTqopqISdRk9bEqzyek2Fhb6464nWRVP70O86WWTPp7gLIk9WPcHEHm6UFiwJJg29b45ypOrTJA1ARbvjfT7m7PI+oZvTBamNJN4JDEERE/n/ADw/p59dUmqSGmBeVkiFaCB8vW8Y74hkT+z6iCJNj3g7D88V8USOp2GMUFJWbKCRZX4q6EmnJW8FhLAb3aZJ+uJFMs9GkM1UOnUyqp7AlywteZJ9cM1yw8FrfgP6HE9zyjUuGU1IhhVH0nWcMsUUIkrH+Z8RKZVvMVqmned7neIixg/0xA1KzMSkhTIgAz+cCxw748Kr0c4aKVGdeJs3lUsbK6zYbdMQ9LNM9HS1JkYPTBlCPwViT9YH5YSUHRVRiyUy9M3R7aepMD89zhzU4aodAyhiWsLdNJse98dcD5jSmagqJqWpSoBbfw001E+7An64l8jxGi+doQLO7aekMopRAmJsxxi4Daip5jKZUSppgaqWojxY63t9N8GQ4Zlno1EUMqu9MMQ4aCWgX9zt1xN8a/s9XacuS4QpOpwCCTMAMPLc33npES75ZpZRqb0qFNl1sjnzEwyuCokk7EAmLWxT1EbtKRk+E0igcK6gPKzUUMJA+YdD5TbD1KTIV0rq2BJdYvFwJv1Nu2EuM5iomWeDpPjUzeLaqbnTcdI/XC/CwzKjMVMosLYnULzAFjFtsLkpcsTah7meCUqlBGeoqMr6mTSDYxAkG+17W1Yj+KUE0aUsD8sCAQYghflC/wCvTDbiddzLRpgXGxgdx02P54SyJqVKlNQIUeaFETbYz6/62xK5S5Zi8DzhGRenBb5rECD3iep+/fa+F+JVmqVSF1eVRqhiZggm0gfqf0xauF8NRctTNdjTINQW2BWD5p6kH39dor3EOI0pNVAfObE2mBZiLSLYRSalur7BL2rglqnAapoCoU0UnKFaj1KcSqlAILA3JPSbjEHnOEZkaIoFoe/ym3cH+mL5TJr5HLJFNho1MrSCCYI0kbbnDDiubamfMKZFiNwYgTJAPWe1ow0G0dixtxpsoFLgeaYhTRKKdyQoFpvY9TO3fFj4Hy5l/Ma+YZKw+WloJUzJFwDeexFrYl6ZqVFlaam0zqPsenfY45z/ADH+zhhUoK7hAyksInUS0ztZYDT1264o7a4Rz5MKS46nv9i5bvT/AM1b/wDlgxoX9hVfT7L/AEwY3Z9Ce0x74n8N8LiGZgfOwcQTfUoYz/zTioZHOKlVNZdUghlAAgmR+IQV2mf1xv3xD4e3jUswp8yU2CL3cEaTOwjUWvvpAxGvkXC0TXmadAF6dyzMslVBgiWECfQ4WeZxk1R1QwQlFNyKFl6dN11r/wAOSFIIBIv5oA3+n63ZZrhVAEs2uwNhe59x1vi61KdSvSqM1BRUFZI/dhSQ4K6V6wDp+84b1eEZdqngeGWqIjyyOw1VVBYgAHYQV9cQUmnyx/k3zTTKIeEqT+7LAFSDr6WsdvfbthqvAKgmCrCABDX3F4IFsX6jwOiQj1C6GowVYg+WYZjaYB27kHCa8FAaoA6otHUXZ0sADH4TYkxGKrMxZaacexQOIcIq6iFViBpusdJnr64V4Rwaq1emrhwpck+0d72xdqfC506KiuXgqRqX5ogQRhb+zWpkFwWB1CdSwSLERO4Pb0jA80qoX0ZquB/nMnTNMIaKsLW1Eex2/wBTiuZrhGXudEC+zmQLQIBJm4v2w9TKktNTxaQUiV8JiCP7pnt1g4QqEB3Ut5blZWDtYGbT7dMQW9KrEyJy5ocZSnRWkYp3AAIJsdwWIF+52P1x1x3iFdxpp0/EUgGZAg3tHXbqLSLYaJRLkBbkKLTp36SYEY44nwvM0YY06mlrBlamYMTcK5b0uAB9sEVO/wDImwb1szndJE1BLahBIOwF2BBI7AnC1JMzURQzV9QmdVRyrW8pi4NypEDcY9bK5pVWo1NgrCV86FmEFvkDFrKJPlEDfbDzguSztVPEopUZASAwdenoWBw7eXwNs+pF1OXqsKGSHjzFp/mxn7DExy/wrRWy9SoY8GqWMC0EAarenpOEOF5rN1WH+71jqBdSFDM6yLkC4MkEyeuFMxxSrSJWqjK4vpamQYuZjeIBM7YnKWVPoGxJ8jDjnAq9YarGp4h3IA0jYm0sxn2EeuPOHcMzFJWhU1li1j3MgKBYR0Fv6O04+5Oylelj06e+FV46dJY0/KGCkibGJ07doOFc8tbaNpPhHrZvOkX+0rE+xBjf9cQPHcixM+aGMtcAT1tp/niwrxym0AbmYBZREAkyWIA2+u2EW4jRqCe/e339cKp5Iu6E2eGVeshYy0mLwGuR/DN+sT/PFmyNJWpRuApIjdbqNJ2MySZiPmg7gcnJUNQllUiIBtvcW9d//GJvh3DmLFaWkuRsIEj64Z5m+iGjCSfKE+J5nxaNSmZXXWLrf+IEaYBHWLdf1jl5ahDJJVyQCtNiBaQP6WGHnMWYOTWn+0U2V6jPpZQG06ApGxA8xbvYLtecTPCKuWzGXSpXrVqLQdVNpOltRWZ03BiQDeMPFz23IpCC6sjsg5pZdKY16gAogSYG3l2G0b4WzGXqrBNRQIBOvSBvtOr5vQThbMZbJ0jrWqXYONAUeaSB0YEH6d8PMlXYQ0mqrUySFZbQZU28oBtefw7Y31CvHcrdXiBC+IKgKzpJCTBOy/ONX0GDO8ENfN5SoVKioyKJAhlN9ViYMMbHDHjIDV28dINS50OTTW0GSCVBgTfbfGl8q+M70qVTLolOgNQbUpOqCo2NrHaOntiim7X1IORe8GCMGOsQhObay0su9c0/EOXBqKsxcDcnsAZPtih8rc5Jnqng5pUpuL0mQMNQ6oSZHa89DjU61IMpVgCrAgg9QbEYyfk7gC5DNZmm1KqfCWUqkA61chVCsux9De57YnkiqbGgr4FcxzsDm3yngAZcVPBUrGsVNQXUSWCi94N498OuaeJrw6lSYUlq16xIXWYCAAagIMlrgWPr7nB+W9AQL47oH1VC/mL9ZDhQDex+YmTftHcS4bU4hnnelX0VcvT1IjrKU9R0hWgyXfzkkfLpWx6J6crqijUUrT+5A8x8WFTL0Wpo4ao3hoxUiApuqH5T5jFiYuLHDbmTjVbwDRrBA71QWZDJYKo8zQOpaAJuQ/bF44lyX+1symu6mgNNEooKI0yWaYLMxhjfoLjbFO5no0aDtQaoK2kjx6z2LPpEoigEAdPT1vjpwafdJWhMmXbFpP7D7J0svRy2YzlAA6B4Qfu+gEN01OS+j0P5c8pcBJioaiVaKUxUqBFgy5kS0nUDDAk7AHFR4jmKtRFoU6aUqFLzpSVj5i1tZLXZo6mNziR5Z4k+Tp5mjOurmEVRSHy3WTVapYWU6YBv7XC5NNJde48NTz7X0/YkOG8SzObquAf3RrgQACJiCw8wYBUM2BkCOmJjJV3rLU0lPDJalRUDz/uWMMWJkl9LWA6i+KfwvPDJVU0HXmjdBJ8OkGWNbC2t9JIA2F74e8KNLK1kfx6prLLU6bHy6ypVqjjaRLe5t64yWklK3FcdBo6qmlJ/Um+E5o1vJ4EkCsq1LQWS46z+Fl+UDa7TbnJc1MKxpVaZqMCRB1z/AMMuG1xAFogkmDNogs+G83VKVTwDGYWmWcajCrc2AESZYWN4n3x7muNAVmqvXHiMdTIgECbHUPwypAiZ63k4PkpxdL7CrUwkuV9f8fqPM7l/HFE1PEedRVBuJgaiylZmOwjfD/M5hMvRpAMjrp0hDIdQAPmkm0deuIL+0iakZdgXQg6iupVF5nvIuB0iemK9xrjFb9paiHgkga3J2IE6j2knHYtBGMvc7X8nPPVucKjGnb/BauDZ56UVDS0IobSdQkrupELJt1jvt1V49mKWZNJ9dQGShKL5jqABUmGJESIjr2nFXo5bwg7A6nhgza9xYGOkHeAL4j+FcUq1j4cqiqLkG5kiwnrc3HYnDv4fjXF030/sJ83KT3VddTX+DvSpUlooKbad0OhiJvLz19Bio8U4LlmzbVXpFUcq3iAEKCfLpVR5WAsbAjeb7xZyrJoSidLVNQGlgST5lIa4iZMm1r2xGrmMyFLU06M7BQABEaiVkW+l98Reg2vmXHcpHVJ29v2LbzBwfKGlro0vOpkim7+cfi0gMfMJDdIxDty6a7aUdE1CVVwwB2mHgzab/wChC1+I1RTpMUAJDHSCx0wQpJBECYFhPrhejzNpaNFxIMny1BpAKm1zE7998bHR49r5vrRks8rSqvJO8I4Rl6hQI60NRtIYh9JIOkCyiRYxB82EOK8OqftBD1XpinUD0QVUPsv7zWPwnSY7fTEfUzNKpDZdvDgjykeYGZu4MMBA6gxA74fVuLeICHqBa+lFZCYFUKWYFTBESxO+023xFaPlNd+vlclXqetpcdPBcufaumnQcVXdy/klkKkNEliJIER0uY2EnFcz2bfM5dMtTVmqrVlYIAa5kxIncWkz0vbEZV5olmp10UJ1mSQLbEdbE7bgb4X4fm6aHXTqjS3mYKIKqjal1AxJtIKkz2nFcekhUozXPZ2SyZ5e3Y7XdUN+Jcu5qjoq1fFoIrgCoIkFoiLyp3FhOPU5VzNVCtKma1NgfOTTWTJBMFh1HYHf698e4vUzah6lbWFnSFJZQSfmUEQIW026zjjlnm1stUfQYVxE/MF9Spm83kH6RiHyUtu6LuvBX5hXtaoi34eMtUOXq0Fp1CA8GGgXgq152Ox3Bxrnwc4YUyzVmMmqQqn+6krIvN21b9hjLatCrma0gmvUeoFFSxMk6BN9QAuLWFyMfRHCcitCjTpLsihfeNz7k3+uHlgjCKd2ybyyk9tUkPcGPcGFAMVP4hZyrRy4q0hKq6+IoFyCQAbXgNE/9sWzCdRAwIIBBsQeuBOgM8bK8QVadNKgNIAQ0AEWFiNzGwIF8LcBomhSqZimniVXZ6dXTckrUcq0+hbaNj6Yd/E/xhkqq0SVZtI1AwQuoajO4EWt3xTvhBRr06tdXhaaUkGldPm8zEE6dm3uZkYo8itRHUG05FxyPDs2tNmp1AHqmagfZSQLrHUCxGM45zyFBM2zKwqGkEVySSNZLEknbXA2JBj2xr+R4ojNUpIwNVZJW50zsTAgC3fGF8x8PNJqjh38TWWZQs+fVckz0Mm49OuN+YeOapC+j6kX9Brm+H61SolUrOoGBII8gAkgiYY26YiMxwllZNFVxrkOSCSI+W/rfbticyQzWaoAClUWgikNVKlUcll8pIWIBm8GTa1sMeI5tkzH71jU/EQhKLJO6iIm1xEX3w3zKcqkvr+grwtK4vjp+pHZXKGjWjUV0td46GxMNHSbHEhmcgGWoyu5ZqkBz/CCIMA2PpO/XGscP5NRsktWmAcxUp66ZdrJq82nyja4BOG68rIaXhZuDmWYFGpsfIpC2ebRqDepB6YPW25P6TfS3Q4/5Ga5xalGii0QNJJBY6ZHy36z9+uBVBotTqKwLFYfwyS1iDLWVbxEgz3EDGi8m8nDMU2qZkGkFqOqIrkk6TDNqYbagQLdJnHQ5Vev4tKjU/dJPgkz5oMhCxBgdNVzjcup9yUeOTMWG4ty7IpXLNUUaalDBCG4En5YJg+hP5YQ4jlGq1W8OlrOwcH/AIbagTPmvYRsRcnDjkPgNSvVY5h2pZbLkrU2DMwMeCG3neStwNoJBxodHgdDMMGoUvCpUmGq5/ejqCYN9u5g77DFNRqoyjth14/YXDg926fTkzdaNUqVIewbYSJG309vphu9Wmq0kRFp1KbsrbFqpc6tZNx5QNAW56jaMXT4kcg0VTxsq3huzgGiWOgzuyCCVI3gCIGwxWsplKlCg1HXTmLNHnGpoBKdN7HVP3jCT1GWW1rraNx4MfuT6UxZ6bMaNnvUAYBYga1G/SxPQRhuvD6qgs3hBaZqSdUs0tGlgAdmFiDb2OIbJZLMeLpqPUIXYmoxVt7e9jbfbfE9ls7RUGlSdvMQDWqaiDKmXCzI6QBvJJxuWWaU4tduaMxRxKEkznOMarJ4at+7TQ5Tb5yQQLfhItf2xF5/KvWCoKNWFqM2phbTI0gQvlgTJm8gxAwhxSklQ6qlVgx/hbzNFtibDa5E9LzhXL52otNaZdyimI1Hp3awNrQOk43J6u5Rj0vz5Mh6e1yl1oYPwcEhprBwmoaVEhRubkSALE/rMYaZXhjVQGSpqJPla+46bSCP9Wgl/wAaq06gW1ZQIuSNIBkEX6TcWI6ezalVemNNIUgpMkH5j/zGwkRhlN79tfgVx9l2KAE+I1fVUZaYVSsD5Wi4G5I73viPztNVqt4TAoYgqTF4kEmJI2uMJ1eF1jozApEU6rPp7Sph1JiBiV1KyFFUKepKmBBi5O9/oLYj6k9ySXFlFCNNtkflS2lYYhhtbt7X/LHWXzBdyNEH0/F/ImT6Y6NcUSuoVUN4MAi/Yg7emHfLPCTm6yUqJLsWUG0aR+JjPRRf6euLvLGD8E9jkaL8F+XvFrPnqgOmnK05My5Hma4/Cpgf4j2xtWI7gXCqeWoU6FMQlNYE7nux9SZJ98SOOTJPfKykVSo9wYMGEGDBgwYAGmeyaVUZHXUGUqfYiDGMXzfAzRpVlLO+jUW1MyqWFlp6VMO0RvNyY2nG5YqPPfLhzCLUSWanc05tUAIbT/i8ojvt2xm231HjPajjkzhSUMqEpQNdR6jQu+tyyz7IVX0gYhuZslRRzUpuTWqsBUFpIiAbDuI2vOK8eb6tUDL5d/D2QgiGkSCAZ8oAERvAi2HOWy70Mxl2cMaZY66hAAJiNIk6iQzai0RCjqcV2Ra9xkZtO4mi/sgfLiiUhWpaWXeJEH0nFB5+yFOjk1DUFenlmUg6rlmhAhO5HmBYneJgRGGnxI5nztDNU8tQrBaVUKyBF89zBDMZ8sgkERaZ2wy4xRarwvN1ajFnpZhkCrEL+8B6SDCnedrdMY8aa8ApUWLgXxEyyZbLoxLV1pqhRe8CY6HbviVC1K9RMydQBjQhEEAHc9PX674+bFzjLDKYKmVPYjrjRqPxTql9VOkBpPlDNIFoOqb+wH8pJursYlfc1RuYP2hv2bLAEwQ7MPkBFwQNm9/++JXh+TTL09OosWYAmP0HQb4+bOP815mrVZmIQHzFacqrNeXIBufeYEDGoct/E7KlMujmu1RUmqSZUOoiZNzqu0iAJ72wrNXPBVOauNNRzOYooAg8aoWPXzuzyBbcMMbLybn6NXLq9IoFj8BMetmAIMzYzj5p47xBs1ma9YTLu7iTcKPlH+UAYm+QeY83RYLTqAUASWVxKz2EQ2o+hA3N8ZFRjYzk5NJk5zpxOtqrFJSmlUhCWZj5jBQajYE3bt0w24fmv3QOoz4fm8/QuJmw62/Ft9cVTmjjtSvUYlpWTH3+Y++I/JZlAra2YMI0gD7+31x1YM7jbkQz41LiLJPjPGqxsrQhYsBM9TBj2tPXDejnmIF9IEbjr7T3w14rTOmk8NDJuRbc7Gf6YRTVSqAVFIuNS9x6dPY4PmPc2hfT4qh1ksy7VPNexF7/AJ/17Yl658qsGXzE3BEiLXFyJ/1GK7mc1LEqNIJJAFo7bdtsS3DqdSrpAgqQS0XYAfNY7Tt9cPjzpRdsyWO3wLftAYQQGmBqO4vPeBtAtt3wpTqgBflgCIN9Uk36gDzb3FrDEA2ebVPYzH8sTOV/elAgAj+GQSbtc3830HthoTx9uxklLuS7ccAp5egLeAaxuAQ3isr3E9IInHOZzFLfQVJhTp+QDc2+m39MQhp66jRuGn1kAmLmJkRc46z+c1SzlpJBNhF/tvH/AJwz23fi31F5qh/xjK60YQGf5l8MSLyQAN/7pjqfvtHwg5GOQy5q1h/vNYAsP/bXcU/fq3rbpJg/hB8P2QrncyrKTejSbpP/AKjjv/CvTc3iNhxx5pqbVeC2OO09wYMGJDhgwYMABgwYMABgwYMAGZ/Ev4dnMzmsn5MyLsgOkVrRv+F4tOx2PcYrXzmeZ/DqeKppnTpckaItHmNvYY+tsVPnTkehnxqaUqqPLUUkT6OARqHbqOh3BLYHzVx7iRNVQrEFBdpJM2Bg77DClLjbplmoLq/eNLgagCNrjboMW3PfDo5at++ViJtJBR9/lYgT3jfvh0vDKYOkUfSYE+pBNiNthhd1KhrbbZR+A5WnWY02hCR5SwldjYyZk2jHmf4JXFQlKcA38sW9gP6Y0VcvSVgVprrAk6VAYA2AIuY36jbDsU1vYWIBi4HW4i2/r0wu7mw7GZZLgdRiDUUwpMyAehtGrvGOs/yvW1F0QaWJOlZtNwPbGnkKVBkQQTq3H+aNQ+n8hjpFUTPcfW392Cf+bG7mZRlfA8hV8ZTpJXZiNhIgSdpmLYm+JcHdUbw0h2HQQSRv0BOL7SKjoJuLb72uLD64KlQQW2UQx2AjqS2xEX6dMY5WFUZI3B6qU21UTtEz1m0eht/XDj+xa7ZdVFO+8ADU17TaYv1jGrCmNvlG11sO2kXHX8sFSmouw3mBeTvIkyB9CMbuMoyqrk8zUpLTakQqRPppER6H8se53gTaATT0sQJaST7QSBjUGAJ6W+gA6GTuR1g48q0ovBIFi32ssxIPo1j3xm41tvqYsnDKpYKFlid+mHWWqVMsfEXvBWD2I33641U8NpGKj0tJ2USQWkTDAEDVvYz745r8GpNZqaMegvAHc7enc4ZysxWjIM9RJYNOov5iAIgmSRviU4coooKsq4I67JNjabk2ExNsXjOckZd2kM4UbgFQPuVn7HCX+wPj/uqCsB6e+5Y7D1M4HJPg1Np2UShnYpstNSHqOZMTY2AWDMzBHrjXvhd8LWDLnOIKS9jSovcrGz1PXsnTrewsnw/+F1DIkVap8fMDZiPLT/wL/F/fN+0XnQ8MZbPcGDBgAMGDBgAMGDBgAMGDBgAMGDBgAMGDBgAb5rLJUUpUUOp3BEjFI43yI2+XYaTY0n6C86X3PSzH64v+DGNWBieZybU20VUIaZCuINuqwbiYv674S8UmCOgJhgdQna026jG1ZjLpUXS6hlPQicVniXIeXqSaZakT2v8Arf8APC7TbM5y+uD5mZgvrqv6/KNvaRjrxiGK+ZmtIUkH3YwARb9cT/EuSMyurQEqpFtBCv6/MAPse+ILiXCMwgbxMvUKx8uk33nzKvW3XC0APUWSR5jIsSBFupiG6n/xhClXdmioiqASASwG97KPm6D/AFGE7ksJJgCKckRE9YG/8sLnLiYY6yfMqsCYK9QSd5Iv0wAL0WCdTJsWiCT0gTG57fTCrVVk2K6j0BBJ6dwbDEc6EGDMkWVm1C3WWYn/AMYVStpPmF221QQDHSTIwAOS5AWAVH4Vki8GQYBXCi1WXfcjyKYAFhaYgn/XfBlcpWqRoouxbqqkqLd9O2JnIcn5kj5UpzuSQOm4UKftbBQEOKzA/h1HqDFp9D0nHdGm5bw6WtydyASe0gqT5vcYunDuR6SgGq5dt20+RSfaT+uLLk8lTpDTTRUHoIn1Pc++GUTCmcL5MdoNZvDXcqsajebkWEnexn0xcsjkadFAlNAijoP598OsGGSoAwYMGNAMGDBgAMGDBgAMGDBgAMGDBgAMGDBgAMGDBgAMGDBgAMGDBgAMGDBjAILmDr7YzLj3znBgwrNGfDevvjSOS/l+uDBgQFuwYMGGMDBgwY0AwYMGAAwYMGAAwYMGAAwYMGAAwYMGAD//2Q=='),
                            backgroundColor: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Text(
                  _title,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff020202),
                  ),
                ),
        ),
        preferredSize: Size.fromHeight(70.0),
      ),
      // bottomNavigationBar: ClipRRect(
      //   borderRadius: BorderRadius.only(
      //     topLeft: Radius.circular(30.0),
      //     topRight: Radius.circular(30.0),
      //   ),
      //   child: BottomNavigationBar(
      //     onTap: onTabTapped,
      //     selectedItemColor: Asset.colorPrimary1,
      //     items: [
      //       BottomNavigationBarItem(
      //         label: "Home",
      //         icon: Icon(Icons.home),
      //       ),
      //       BottomNavigationBarItem(
      //         label: "Explore",
      //         icon: Icon(Icons.explore),
      //       ),
      //       BottomNavigationBarItem(
      //         label: "History",
      //         icon: Icon(Icons.history),
      //       ),
      //     ],
      //   ),
      // ),
      body: _widgetOptions.elementAt(_selectedIndex),
    );
  }

  // void onTabTapped(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //     switch (index) {
  //       case 0:
  //         {
  //           _title = 'default';
  //         }
  //         break;
  //     }
  //   });
  // }
}

class IconButton extends StatelessWidget {
  final String nameLabel;
  final IconData iconLabel;

  IconButton(this.nameLabel, this.iconLabel);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            // margin: EdgeInsets.only(bottom: 5),
            child: Material(
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
              color: Colors.transparent,
              child: InkWell(
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
                onTap: () {},
                child: Container(
                  // margin: EdgeInsets.all(5),
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ),
                  child: Center(
                    child: Stack(
                      children: [
                        Icon(
                          iconLabel,
                          color: Colors.white,
                          size: 40,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Text(
              nameLabel,
              style: TextStyle(fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}
