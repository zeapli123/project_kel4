import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/src/rendering/box.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:project_kelas/screen/admin/dashboard_admin.dart';
import 'package:project_kelas/screen/admin/Profil.dart';
import 'package:project_kelas/screen/login.dart';
import 'package:project_kelas/Loginpage/login_google_page.dart';
import 'package:project_kelas/screen/admin/detail_page.dart';
import 'package:project_kelas/home_page.dart';

class ButtomBar extends StatefulWidget {
  const ButtomBar({Key? key}) : super(key: key);

  @override
  State<ButtomBar> createState() => _ButtomBarState();
}

class _ButtomBarState extends State<ButtomBar> {
  int currentIndex = 0;
  final List<Widget> body = [
    DashboardAdmin(),
    HomePage(),
    DetailPage(),
    Profil(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body[currentIndex],
      // bottomNavigationBar: BottomNavigationBar(items: [
      //
      //   BottomNavigationBarItem(icon: icon)
      // ]),

      bottomNavigationBar: CurvedNavigationBar(
        height: 55,
        buttonBackgroundColor: Color.fromARGB(255, 224, 171, 11),
        backgroundColor: Color.fromARGB(120, 49, 2, 2),
        animationCurve: Curves.easeOutExpo,
        items: <Widget>[
          Icon(
            Icons.home,
            size: 30,
          ),
          Icon(Icons.bookmark, size: 30),
          Icon(Icons.favorite, size: 30),
          Icon(Icons.person, size: 30),
        ],
        onTap: ontap,
        index: currentIndex = currentIndex,
      ),
    );
  }

  void ontap(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
